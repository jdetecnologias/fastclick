<?php
if($dados == false){
   echo "Não foi encontrado nenhum resultado para está pesquisa!"; 
}
else{
  
  ?>
<div id="resultadoGeral" class="col-md-8 col-md-offset-2 caixaPadrao">
    <div id="fotoEmpresa" class="col-md-3 caixaDadosEmpresaHead">
        <img src="<?php echo base_url('/img/perfilEmpresaNull.png');?>"/>
    </div>
    <div id="nomeEmpresa" class="col-md-5 caixaDadosEmpresaHead">
        <?php echo $dados['nome'];?>
    </div>
     <div id="fotoEmpresa" class="col-md-4 caixaDadosEmpresaHead">
        <img src="<?php echo base_url('/img/twitter.png');?>"/>
    </div>
    <div id="endEmpresa" class="col-md-12 caixaDadosEmpresa">
        <span>Endereço: </span><span><?php echo $dados['end'];?></span>
    </div>
    <div id="telEmpresa" class="col-md-12 caixaDadosEmpresa">
        <span>Telefone: </span><span><?php echo $dados['tel'];?></span>
    </div>
     <div id="horarioEmpresa" class="col-md-12 caixaDadosEmpresa">
        <span>Horario de funcionamento: </span><span>08:00 às 18:00 hrs</span>
    </div>
    <div id="descEmpresa" class="col-md-12 caixaDadosEmpresa">
        <span>Descriçâo: </span><span><?php echo $dados['desc'];?></span>
    </div>
    <div class="col-md-12">
        <img src="<?php echo base_url('/img/mapa.png');?>"/>
    </div>
</div>
<?php 
}
?>