
<div class="col-md-8 col-md-offset-2">
  <div class="col-md-8">
        <div class="fonteTitulos text-center">Entre em contato</div>
        <div class="col-md-12">
          <form id="cadastroEmpresa" method="post" class="form-horizontal formPadrao"  action="<?php echo base_url('/index.php/acao/cadastrar'); ?>">
            
            <div class="control-group">
              <label class="control-label" for="inputNome">Nome</label>
              <div class="controls">
                <input id="nome" type="text" placeholder="" name="nome" value="<?php echo set_value('nome');?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputEmail">E-mail</label>
              <div class="controls">
                <input id="email" type="text" placeholder="" name="email" value="<?php echo set_value('email');?>"/>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label" for="inputTelefone">Telefone</label>
              <div class="controls">
                <input id="telefone" type="text" placeholder="" name="telefone" value="<?php echo set_value('telefone');?>"/>
              </div>
            </div>
             
             <div class="control-group">
              <label class="control-label" for="inputDescricao">Descrição</label>
              <div class="controls">
                <textarea id="descricao" type="text" placeholder="" name="descricao"></textarea>
              </div>
            </div>  
            <div class="control-group">
              <div class="controls">
                <button class="btn" type="submit">Enviar</button>
                <button class="btn" type="reset">Limpar</button>
              </div>
            </div>
          </form>
      </div>
  </div>
  
  <div class="col-md-4">
      <?php
      if (isset($erros)){
      echo $erros;
      }
      ?>
    
  </div>

</div>

