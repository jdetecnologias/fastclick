<script>
categorias = new Array();
</script>

<div class="col-md-8 col-md-offset-2">
  <div class="col-md-8 caixaPadrao">
        <div class="fonteTitulos text-center">Cadastre-se gratís</div>
        <div class="col-md-12 ">
          <form id="cadastroEmpresa" method="post" class="form-horizontal formPadrao"  action="<?php echo base_url('/Cadastrar/cadastrando'); ?>">
            <div class="control-group">
              <label class="control-label" for="inputNome">Razão Social ou nome</label>
              <div class="controls">
                <input id="nome" type="text" placeholder="" name="nome" value="<?php echo set_value('nome');?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputEmail">E-mail(Usuário para login)</label>
              <div class="controls">
                <input id="email" type="text" placeholder="" name="email" value="<?php echo set_value('email');?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputSenha">Senha</label>
              <div class="controls">
                <input id="senha" type="password" placeholder="" name="senha"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputCategoria">Confirmar senha</label>
              <div class="controls">
                <input id="confSenha" type="password" placeholder="" name="confSenha"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputEndereco">CEP</label>
              <div class="controls">
                  <input id="cep" type="text" placeholder="" name="cep" value="<?php echo set_value('endereco');?>"/>
              </div>
              <label class="control-label" for="inputEndereco">Logradouro</label>
              <div class="controls">
                  <input id="logradouro" type="text" placeholder="" name="logradouro" value="<?php echo set_value('endereco');?>"/>
              </div>
              <label class="control-label" for="inputEndereco">Número</label>
              <div class="controls">
                  <input id="numero" type="text" placeholder="" name="numero" value="<?php echo set_value('endereco');?>"/>
              </div>
              <label class="control-label" for="inputEndereco">Complemento</label>
              <div class="controls">
                  <input id="complemento" type="text" placeholder="" name="complemento" value="<?php echo set_value('endereco');?>"/>
              </div>
              <label class="control-label" for="inputEndereco">Bairro</label>
              <div class="controls">
                  <input id="bairro" type="text" placeholder="" name="bairro" value="<?php echo set_value('endereco');?>"/>
              </div>
               <label class="control-label" for="inputEndereco">Cidade / Estado</label>
              <div class="controls">
                <select id="estado" name="estado">
                  <option value="GO">GO</option>
                </select>
                   <select id="cidade" name="cidade">
                  <option value="senador_canedo">Senador Canedo</option>
                </select>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label" for="inputTelefone">Telefone</label>
              <div class="controls">
                <input id="telefone" type="text" placeholder="" name="telefone" value="<?php echo set_value('telefone');?>"/>
              </div>
            </div>
             <div class="control-group">
               
              <label class="control-label" for="inputCategoria">Categoria</label>
               <div id="categorias" >
                <div id="t">
                  
                 </div>
                </div> 
              <div class="controls">
                <select id="categoria" name="categoria">
                  <option>Selecione a Categoria de seu negócio</option>
                  <?php
                  $query = $this->db->query('select*from categorias');
                    if($query || $query->num_rows()>0){
                      foreach($query->result() as $lista){
                          echo "<option value='".$lista->id."'>".$lista->nome."</option>";
                      }
                    }
                  ?>
                </select>
               <!-- <input id="categoria" type="hidden" name="categoria" value="<?php echo set_value('categoria');?>">
                <input  id="categoria1"type="text" placeholder="Incluir Categoria"/>
                
                <div id="categoriaEncontradas" class="btn-info pull-left col-sm-9" style="position:absolute;">
                  
                </div>-->
               </div>
                 
            </div>
            
             <div class="control-group">
              <label class="control-label" for="inputDescricao">Descrição</label>
              <div class="controls">
                <textarea id="descricao" type="text" placeholder="" name="descricao"></textarea>
              </div>
            </div>  
            <div class="control-group">
              <div class="controls">
                <button class="btn" type="submit">Gravar</button>
                <button class="btn" type="reset">Limpar</button>
              </div>
            </div>
          </form>
      </div>
  </div>
  
  <div class="col-md-4">
      <?php
      if (isset($erros)){
      echo $erros;
      }
      ?>
    
  </div>

</div>

