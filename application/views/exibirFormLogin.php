<div class="col-md-8 col-md-offset-2">
  <div class="col-md-8 caixaPadrao">
    <div class="fonteTitulos text-center">Login</div>
    <div class="col-md-12">
      <form id="cadastroEmpresa" method="post" class="form-horizontal formPadrao" action="<?php echo base_url('Logar/logando'); ?>">

        <div class="control-group">
          <label class="control-label" for="inputNome">Usuário(E-mail)</label>
          <div class="controls">
            <input id="email" type="text" placeholder="" name="email" value="<?php echo set_value('email');?>" />
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputSenha">Senha</label>
          <div class="controls">
            <input id="senha" type="password" placeholder="" name="senha" />
          </div>
        </div>
        <div class="control-group">
          <div class="controls">
            <button class="btn" type="submit">Enviar</button>
            <button class="btn" type="reset">Limpar</button>
          </div>
        </div>
      </form>

    </div>
  </div>

    <div class="col-md-4">
      <?php
      if (isset($erros)){
      echo $erros;
      }
      ?>

    </div>

  
</div>