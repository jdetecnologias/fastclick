
<div id="resultadoGeral" class="col-md-6 panel-group col-md-offset-3">
    <?php 
    if($dado == false){
        echo "Não foi encontrado nenhum resultado para está pesquisa!";
    }
    else{
        foreach($dado as $dados){
            ?>
        
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="fotoEmpresa" class="col-md-3">
                    <img src="<?php echo base_url('/img/perfilEmpresaNull.png');?>"/>
                </div>
                <div id="nomeEmpresa" class="col-md-5 ">
                  <a href="<?php echo base_url('/ExibirEmpresa/empresa/').$dados['id'];?>" > <?php echo $dados['nome'];?></a>
                </div>
                 <div id="redesSociais" class="col-md-4 ">
                    <img src="<?php echo base_url('/img/twitter.png');?>"/>
                </div>
                <div id="endEmpresa" class="col-md-12 ">
                    <span>Endereço: </span><span><?php echo $dados['end'];?></span>
                </div>
                <div id="telEmpresa" class="col-md-12 ">
                    <span>Telefone: </span><span><?php echo $dados['tel'];?></span>
                </div>
                 <div id="horarioEmpresa" class="col-md-12 ">
                    <span>Horario de funcionamento: </span><span>08:00 às 18:00 hrs</span>
                </div>
                <div id="descEmpresa" class="col-md-12 caixaDadosEmpresa">
                    <span>categoria: </span><span><?php echo $dados['cat'];?></span>
                </div>
            </div>
        </div>
    <?php
        }
    }
    ?>
</div>
