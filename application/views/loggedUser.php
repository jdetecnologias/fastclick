<html lang="en">
<head>
	<title>fast-click o que desejar há um clique de distância!</title>
<meta name="viewport" content="width=device-width, initial-scale=1">	
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/bootstrap.min.css");?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/style.css");?>"/>
<link rel="stylesheet" href="<?php echo base_url("css/font-awesome.min.css");?>"/>
<script type="text/javascript" language="javascript" src="<?php echo base_url("js/jquery-3.1.1.min.js");?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url("js/script.js");?>"></script>
<script type="text/javascript" language="javascript">
	
	$(document).ready(function(){

		function viewResult(numChar,Char){
			
    		if(numChar>3){ 
    			
            	$.post('<?php echo base_url('index.php/Acao/motorPesquisa');?>',{Char:Char},function(a){
            	
				 $("#resultado").show();
				 $("#resultado > .panel-body").html(a);
                });
            }
            else{
                $("#resultado").hide();
            }  
		}
		
		
		$("#pesquisaLocal").on('focus',function(){
			 Char = $(this).val();
             numChar = Char.length;
			viewResult(numChar,Char);
			
		});
     
    		$("#pesquisaLocal").on('keyup',function(e){
    		
            	Char = $(this).val();
             numChar = Char.length;
             link = $(".icone-buscar").parent().attr("link");
    		newHref = link+"/"+Char;
    		$(".icone-buscar").parent().attr("href",newHref);
    		
    		$(this).attr("href",newHref);
                 viewResult(numChar,Char);              
    		});
    	
    	$("div:not(#resultado)").on('click',function(){
    		$("#resultado").hide();
    	});
    	
    	$(".icone-buscar").on('click',function(){
    		
    	});

});
</script>
<body>
	<nav id="painel-control" class="navbar navbar-default navbar-fixed-top no-padding no-border fundoPadrao2">
	
		<div class="container-fluid">
		<div id="logo" class="col-sm-1 col-xs-3 no-padding navbar-header">
							<a class="navbar-brand no-padding no-margin" href="<?php echo base_url();?>"><img src="<?php echo base_url("img/logo.png");?>" class="logotipo"/></a>
		</div>
			<div class="col-sm-5 col-xs-8" id="controlePesquisa">
				<form class="navbar-form no-padding" id="formPesquisa">
					<div id="caixaPesquisa" class="col-xs-12 form-group no-padding">
						<div  class="col-xs-11 form-group">
							<input type="text" placeholder="Pesquisar Local" class="col-md-10" id="pesquisaLocal"/>
						</div>
					
				
						<a class="col-xs-1 no-padding" href="<?php echo base_url('/Acao/exibirResults');?>" link="<?php echo base_url('index.php/Acao/exibirResults');?>">
							<div id="iconeBuscar" class="glyphicon glyphicon-search icone-buscar no-padding visible-md visible-xl visible-lg">
							</div>	
						</a>
					</div>
					</form>	
				<div  id="resultado" class="resultado panel panel-default no-padding">
					<div class="panel-body"></div>
					<div class="panel-footer"></div>
				</div>
			
		</div>
		<button class="navbar-toggle no-margin-horizon no-border" data-toggle="collapse" data-target="#menuDireita">
       	<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
		
			<div class="col-sm-6 col-xs-6 col-xs-offset-6 col-sm-offset-4 col-md-offset-0 no-padding">
				
				<ul class="nav navbar-nav navbar-right collapse navbar-collapse fundoPadrao2" id="menuDireita">
					<li><a href="<?php echo base_url();?>">Home</a></li>
					<li><a href="<?php echo base_url('acao/cadastro');?>"><span class="glyphicon glyphicon-user"></span>Cadastrar</a></li>
					<li><a href="#"><span class="glyphicon glyphicon-cog"></span>Configuração</a></li>
					<li> <?php if($this->session->userdata('logged_in')==true){echo "<a href='".base_url('acao/logout')."'><span class='glyphicon glyphicon-log-out'></span>logout</a>";}else{?><a href="<?php echo base_url('acao/login');?>"><span class="glyphicon glyphicon-log-in"></span>Login</a><?php }?></a></li>
				</ul>
	
			</div>
			
		</nav>
		
			
				<div id="conteudoPrincipal" class="floatLeft row col-xs-12 no-padding">
					<div id="colunaEsquerda" class="col-sm-2">
						<div id="fotoPerfil" class="col-sm-8 col-sm-offset-2">
								FOTO PERFIL
						</div>
							
					</div>
					<div id="desktop" class="col-sm-7">
						Bem vindo <?php echo $this->session->userdata('username');?>
					</div>
					<div id="colunaDireita" class="col-sm-3">
						fotos e videos
					</div>
			</div>
			<div class="row visible-md visible-sm visible-xl visible-lg">
				<?php include_once("./include/rodape.php");?>
			</div>	
		
	</div>	
											
</body>
</html>
