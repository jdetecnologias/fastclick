<html lang="pt-br">
<head>
	<title><?php if(!isset($title)){echo "fast-click o que desejar há um clique de distância!";} else{ echo $title;}?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">	
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/bootstrap.min.css");?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/style.css");?>"/>
<link rel="stylesheet" href="<?php echo base_url("css/font-awesome.min.css");?>"/>
<script type="text/javascript" language="javascript" src="<?php echo base_url("js/jquery-3.1.1.min.js");?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.1/jquery.form.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url("js/script.js");?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.0/jquery.nicescroll.min.js"></script>	
<script type="text/javascript" language="javascript">

	$(document).ready(function(){
		$("#postagens").niceScroll();
		$("#menu-principal").niceScroll();
		$("#file").on("change",function(){
			alert("hello isn't me you looking for");
			$('#preview').html("<img src='/img/loading.gif'>");
			$("#upload").ajaxForm({target:"#preview"}).submit();
			$("#file").val("");	
			;
		});
		
		$("#categoria1").on('keyup',function(e){
			Char = $(this).val();
			verCategoria(Char);
		});
		
		function verCategoria(Char){
			numChar = Char.length;
			if(numChar > 2){
			$.post('<?php echo base_url('/ObterCategoria/categoria');?>',{Char:Char},function(a){   	
							$("#categoriaEncontradas").show();
							$("#categoriaEncontradas").html(a);
								
                });
				}
			else{
							$("#categoriaEncontradas").html("");
			}
		}

		function viewResult(numChar,Char){
			
    		if(numChar>3){ 
					
         $.post('<?php echo base_url('/Autocompletar/autocompletarPesquisa');?>',{Char:Char},function(a){   	
			
					 $("#resultado").show();
				 $("#resultado > .panel-body").html(a);
                });
            }
            else{
                $("#resultado").hide();
            }  
		}
		
		
		
			$(document).on('click','.chooseCat',function(e){
				
				categoria = $(this).html();
			idCategoria = $(this).attr('id');
					categorias.push(idCategoria);
				indice = (categorias.length-1);
				$("#categoria").val(categorias);
			codigo = "<div class='btn btn-info no-padding' indice ='"+indice+"' id='"+idCategoria+"'>"+categoria+"</div>";
			$("#categorias").append(codigo);	
		});
		
		$(document).on('click','#categorias div.btn',function(e){
			ind = $(this).attr("indice");
			categorias.splice(ind,1);
			$("#categoria").val(categorias);
			
			
			$(this).remove();
		});
		$("#pesquisaLocal").on('focus',function(){
			 Char = $(this).val();
             numChar = Char.length;
			viewResult(numChar,Char);
			
		});
     $("#categoria").on('focus',function(){
			 Char = $(this).val();
       numChar = Char.length;
				verCategoria(Char);
			
		});
    		$("#pesquisaLocal").on('keyup',function(e){
    		
            	Char = $(this).val();
             numChar = Char.length;
             link = $(".icone-buscar").parent().attr("link");
    		newHref = link+"/"+Char;
    		$(".icone-buscar").parent().attr("href",newHref);
    		
    		$(this).attr("href",newHref);
                 viewResult(numChar,Char);              
    		});
    	
    	$("div:not(#resultado)").on('click',function(){
    		$("#resultado").hide();
    	});
			$("div:not(#categoriaEncontradas)").on('click',function(){
    		$("#categoriaEncontradas").hide();
    	});
		
    	
    	$(".icone-buscar").on('click',function(){
    		
    	});
		
		$("button.publicar").on("click",function(){
    var texto = $("#publicacao").val();
	
		if(texto == ""){
		
		}
		else{	
			var data = new Date();
			var horaAtual = data.getHours()+":"+data.getMinutes()+":"+data.getSeconds();
	
			$.post('<?php echo base_url("Acao/publicar");?>',{texto:texto,data:horaAtual},function(e){
				alert(texto);
				$("#publicacao").val("");
				$("#postagens").hide().fadeIn(1000).html(e);
			});
		}
  });
		
		$(document).on("click","#editar-publicacao",function(){
			
			var textContent = $(this).parents(".raiz").find(".texto-publicacao");
			var texto = textContent.html();
			var editor = "<textarea id='publicacao'>"+texto+"</textarea>";
			var button = "<div class='col-sm-2 col-sm-offset-8 no-padding'><button class='btn btn-link submit-form no-padding-vertical'>Enviar</button></div>";
			textContent.html(editor);
			textContent.next().html(button);
			
			
		});
			$(document).on("click","#deletar-publicacao",function(){
			
			var textContent = $(this).parents(".raiz").find(".texto-publicacao");
				var id = textContent.attr("id-publicacao");
					$.post('<?php echo base_url("/Acao/deletarPublicacao");?>',{id:id},function(e){
				$("#postagens").hide().fadeIn(1000).html(e);
			});
		});
	
		$(document).on("click",".submit-form",function(){
			
			var texto = $(this).parents(".raiz").find(".texto-publicacao");
			
			var id = texto.attr('id-publicacao');
			
			var textContent = texto.children("textarea").val();
			
			$.post('<?php echo base_url("/Acao/atualizarPublicacao");?>',{text:textContent,id:id},function(e){
				$("#postagens").hide().fadeIn(1000).html(e);
			});
		});

});
	
	
</script>
<body class="bg-info">
	
	<nav id="painel-control" class="navbar navbar-default navbar-fixed-top no-padding no-border fundoPadrao2">
		<div class="container-fluid">
		<div id="logo" class="col-sm-1 col-xs-3 no-padding navbar-header">
							<a class="navbar-brand no-padding no-margin" href="<?php echo base_url();?>"><img src="<?php echo base_url("img/logo.png");?>" class="logotipo"/></a>
		</div>
			<div class="col-sm-5 col-xs-8" id="controlePesquisa">
				<form class="navbar-form no-padding" id="formPesquisa">
					<div id="caixaPesquisa" class="col-xs-12 form-group no-padding">
						<div  class="col-xs-11 form-group">
							<input type="text" placeholder="Pesquisar Local" class="col-md-10" id="pesquisaLocal"/>
						</div>
							<a class="col-xs-1 no-padding" href="<?php echo base_url('/Acao/exibirResults');?>" link="<?php echo base_url('index.php/Acao/exibirResults');?>">
							<div id="iconeBuscar" class="glyphicon glyphicon-search icone-buscar no-padding visible-md visible-xl visible-lg">
							</div>	
						</a>
					</div>
					</form>	
				<div  id="resultado" class="resultado panel panel-default no-padding">
					<div class="panel-body"></div>
					<div class="panel-footer"></div>
				</div>
			
		</div>
		<button class="navbar-toggle no-margin-horizon no-border" data-toggle="collapse" data-target="#menuDireita">
       	<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
		
			<div class="col-sm-6 col-xs-6 col-xs-offset-6 col-sm-offset-4 col-md-offset-0 no-padding">
				
				<ul class="nav navbar-nav navbar-right collapse navbar-collapse fundoPadrao2" id="menuDireita">
					<li class="btn-danger"><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-usd"></span> Promoção</a></li>
					<li class="btn-warning"><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li class="btn-danger"><a href="<?php echo base_url('acao/cadastro');?>"><span class="glyphicon glyphicon-user"></span>Cadastre-se gratís</a></li>
					<li class="btn-warning"> <?php if($this->session->userdata('logged_in')==true){echo "<a href='".base_url('acao/logout')."'><span class='glyphicon glyphicon-log-out'></span> Logout</a>";}else{?><a href="<?php echo base_url('acao/login');?>"><span class="glyphicon glyphicon-log-in"></span> Login</a><?php }?></a></li>
				</ul>
	
			</div>
		</nav>	
		
			<?php 
						$statusLogado = $this->session->userdata('logged_in');
						if($statusLogado == false){
						$this->load->view('vDesktop.php',array('paginaInicial'=>'paginaInicial.php'));
						}	
						else{
						$this->load->view('vDesktop.php',array('paginaInicial'=>'paginaInicialPerfil.php','title'=>'Pagina de Perfil'));	
						}
					?>
			
			
			
			<div class="fundoPadrao2 col-sm-12 no-padding visible-md visible-sm visible-xl visible-lg" id="rodape">
					<div class="col-sm-12">
						 <div class="col-sm-4 col-sm-offset-4">
							Contato: suporte@fastclick.com.br ou clique aqui.</br>
						</div>
					</div>
					<div class="col-sm-12">	
						<div class="col-sm-9">Termo de uso| Politica de privacidade | <a href="<?php echo base_url('acao/contato');?>">Contato </a>|<a href="<?php echo base_url('acao/cadastro');?>"> Cadastre-se gratís</a>
						</div>
						<div class="col-sm-3 text-right">Todos os direito de copyright.
						</div>
					</div>	
			</div>	
		
	</div>	
											
</body>
</html>
