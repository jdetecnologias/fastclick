<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastrar extends CI_Controller {
	

	public function cadastrando(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome','Nome ou Razão Social','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('email','E-mail ','required|valid_email',array('required'=>'O %s é obrigatórtio o preenchimento!','valid_email'=>' Favor ppreencher um e-mal correto.'));
		$this->form_validation->set_rules('telefone','Telefone','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('cep','cep','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('logradouro','Logradouro','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('numero','Numero','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('complemento','Complemento','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('bairro','Bairro','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('complemento','Complemento','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('categoria','Categoria','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('descricao','Descrição','required',array('required'=>'O %s é obrigatórtio o preenchimento!'));
		$this->form_validation->set_rules('senha','Senha','required|min_length[6]|max_length[10]',array('required'=>'A senha é necessária','min_length[6]'=>'Senha com no mínimo 6 caracteres','max_length[10]'=>'Senha com no maxímo 10 caracteres'));
		$this->form_validation->set_rules('confSenha','Senhas não confere','matches[senha]');
		if($this->form_validation->run() === FALSE){
			$erros = validation_errors();
			$this->load->view('vIndex',array('retorno'=>1,'erros'=>$erros));
			
		}
		else{
			$this->load->model('cadastroEmpresas');
			$dados['nome'] = $this->input->post('nome');
			$dados['telefone'] = $this->input->post('telefone');
			$dados['descricao'] = $this->input->post('descricao');
			$dados['senha'] = $this->input->post('senha');
			$dados['email'] = $this->input->post('email');
			$dados['logradouro'] = $this->input->post('logradouro');
			$dados['numero'] = $this->input->post('numero');
			$dados['complemento'] = $this->input->post('complemento');
			$dados['bairro'] = $this->input->post('bairro');
			$dados['cidade'] = $this->input->post('cidade');
			$dados['estado'] = $this->input->post('estado');
			$dados['cep'] = $this->input->post('cep');
			$categoria['idCategoria'] = $this->input->post('categoria');
			$cadastrar = new CadastroEmpresas();
			$cadastrar->gravarDados($dados,$categoria);
				$this->load->view('vIndex',array('retorno'=>7,'status'=>1,'mensagem'=>'Cadastro realizado com sucesso!'));
				
		}
	}
	
}
