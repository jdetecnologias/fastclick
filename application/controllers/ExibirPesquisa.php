<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExibirPesquisa extends CI_Controller {

		public function categoria($id)
		{
			
			$this->load->model('MexibirPesquisa');
			$this->load->model('MexibirEmpresa');
			$pesquisa = new MexibirPesquisa();
			$listaEmpresa = $pesquisa->categoria($id);
			if($listaEmpresa == false){
				$dados = false;
			}
			else{
			$i = 0;
				foreach($listaEmpresa as $idE){
					$obterEmpresa = new MexibirEmpresa();
					$resultado = $obterEmpresa->exibirEmpresa($idE->idEmpresa);
					if($resultado == false){
						$dados = false;
					}
					else{
					foreach($resultado as $row){
					$dados[$i] = array('id'=>$row->id_usuarios,'nome'=>$row->nome,'end'=>$row->logradouro,'tel'=>$row->telefone,'cat'=>"2");
					$i++;
					}
					}
				}
			}
			$retorno = array('retorno'=>3,'dado'=>$dados);
			$this->load->view('vIndex',$retorno);
		}	
}
