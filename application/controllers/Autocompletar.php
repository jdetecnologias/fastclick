<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autocompletar extends CI_Controller {

		public function autocompletarPesquisa(){
		$dados = $this->input->post('Char');
		
		$this->load->model('AutocompletarPesquisa');
		$pesquisar = new AutocompletarPesquisa();
		$contadorCat = $pesquisar->mostrarCategoria($dados);
		$contadorNome = $pesquisar->mostrarPorNome($dados);
		if($contadorCat > 0){
			$resultadoPesquisa = $pesquisar->mostrarDados($dados,'categorias','nome');
			foreach($resultadoPesquisa as $row){
			echo "<div><a href='".base_url('/ExibirPesquisa/categoria/'.$row->id)."'>".$row->nome."</a></div>";
			}
		}
		else{
			
			$resultadoPesquisa = $pesquisar->mostrarDados($dados,'usuarioEmpresas','nome');
			if($resultadoPesquisa == false){
				echo "<div>Sem resultados</div>";
			}
			else{
				foreach($resultadoPesquisa as $row){
			echo "<div><a href='".base_url('ExibirEmpresa/empresa/'.$row->id)."'>".$row->nome."</a></div>";
			}
			}
		}
		
		
		
	}
	
}
