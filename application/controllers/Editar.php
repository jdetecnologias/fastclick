<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

	public function perfil(){
		if($this->session->userdata('logged_in') == true){
			$this->load->view('vIndex',array('retorno'=>6));
		}
		else{
			header("location:".base_url());
		}	
	}
}
