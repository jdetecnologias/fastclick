<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cupload extends CI_Controller {
	public function upload(){
		$config['upload_path']          = 'application/uploads/img/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 1024;
    $config['max_width']            = 3000;
    $config['max_height']           = 3000;
		$config['file_name']						=	md5(uniqid(time()));
		$this->load->library("upload",$config);		
    if(!$this->upload->do_upload('file')){
			echo $this->upload->display_errors();
		}
		else{
			$setting['image_library'] = 'gd2';
			$setting['source_image'] 		= $this->upload->data('full_path');
			$setting['create_thumb'] 		= FALSE;
			$setting['new_image'] 			= 'application/uploads/thumb/';
			$setting['maintain_ratio'] 	= TRUE;
			$setting['width']         	= 150;
			$setting['height']       		= 100;
			$this->load->library("image_lib",$setting);
			if(!$this->image_lib->resize()){
			echo $this->image_lib->display_errors();
			}
			else{
				date_default_timezone_set('America/Sao_Paulo');
					$dados = array(
					"nome"=>$this->upload->data('file_name'),
					"idUser"=>$this->session->userdata('id'),
					"data"=>date('Y-m-d H:i:s',time('America/sao_paulo'))
					);
					$this->load->model('Mupload');
					$gravar = new Mupload();
					$resultado = $gravar->gravarImg($dados);
					if(!$resultado){
						echo "não foi salvo no BD";
					}
					else{
						echo "Imagem salva com sucesso";
					}
			}
		
		
   
			}
	}
}


