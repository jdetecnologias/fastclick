<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acao extends CI_Controller {


	public function cadastro()
	{
		$action = array('retorno'=>1);
		$this->load->view('vIndex',$action);
	}
	public function gravarCAt(){
		$this->load->model('MgravarCategoria');
		$garv = new MgravarCategoria;
		$string = 'Restaurantes.Doceria .Panificadora.padaria.lanchonete';
		$control = $garv->gravar($string,1);
			if($control == true){
				echo "<script>alert('cadastrou')</script>";
			}	else{
				echo "<script>alert('Não cadastrou')</script>";
			}
		
	}
	
	public function contato(){
		$dados = array("retorno"=>4);
		$this->load->view('vIndex',$dados);
		
	}
	public function login(){
		$dados = array("retorno"=>5);
		$this->load->view('vIndex',$dados);
		
	}
	public function logout(){
		$this->session->sess_destroy();
		
		header("location:".base_url());
	}
	
	public function publicar(){
		
		$this->load->model("Mpublicar");
		$texto = $this->input->post('texto');
		$data = $this->input->post('data');
		
		
		$id = $this->session->userdata('id');
		$publicar = new Mpublicar();
		$sucesso = $publicar->publicarTexto($texto,$id,$data);
		$this->load->view("vPostagens",array('id'=>$this->session->userdata("id")));
	
	}
	
	public function atualizarPublicacao(){
		
		$this->load->model("Mpublicar");
		$text = $this->input->post('text');
		$id = $this->input->post('id');
		$atPublicar = new Mpublicar();
		$sucess = $atPublicar->atualizarPublicacao($id,$text);
		if($sucess){
			$this->load->view("vPostagens",array('id'=>$this->session->userdata("id")));
		}
		else{
			echo "erro";
		}
	
	}
	
	public function deletarPublicacao(){
		
		$this->load->model("Mpublicar");
		$id = $this->input->post('id');
		$atPublicar = new Mpublicar();
		$sucess = $atPublicar->deletarPublicacao($id);
		if($sucess){
			$this->load->view("vPostagens",array('id'=>$this->session->userdata("id")));
		}
		else{
			echo "erro";
		}
	
	}
	

}

