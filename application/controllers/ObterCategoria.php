<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ObterCategoria extends CI_Controller {
	public function categoria(){
		$Char = $this->input->post('Char');
		
		$this->load->model('MpegarDados');	
		$getTable = new MpegarDados();
		$dataTable = $getTable->categoria($Char);
		if($dataTable == false){
			echo "<div>Não foi encontrado a categoria</div>";
		}
		else{
			foreach($dataTable as $d){
				echo "<div class='col-sm-12 chooseCat' id='".$d->id."'>".$d->nome."</div>";
			}
			}
	
	
	}

}
