<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logar extends CI_Controller {

	public function logando(){
		$this->load->model('logarSistema');
		$dados['email'] = $this->input->post('email');
		$dados['senha'] = $this->input->post('senha');

		$logar = new LogarSistema();
		$status = $logar->logarSe($dados);
		
		if($status == false || $dados['email'] == "" || $dados['senha'] == ""){
						header("location:".base_url());
		}
		else{
			foreach($status as $dados){
				$usuario =  $dados->nome;
				$id = $dados->id;
				$telefone = $dados->telefone;
				$enderecoCompleto = $dados->logradouto.", ".$dados->numero.", ".$dados->complemento.", CEP ".$dados->cep.", ".$dados->cidade." - ".$dados->estado;
			}
			$newUser = array('username'=>$usuario,'id'=>$id,'logged_in'=>true,'endereco'=>$enderecoCompleto,'telefone'=>$telefone);
			$this->session->set_userdata($newUser);
			$this->load->view('vIndex',array('retorno'=>0));
			header("location:".base_url());
		}
	}	
}
