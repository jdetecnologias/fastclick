<?php

class Mpublicar extends CI_Model{
	
	public function publicarTexto($texto,$id_user,$data){
		
		$this->load->helper('date');
		$stringdedata = "%Y-%m-%d";
		$date = mdate($stringdedata,time())." ".$data;
		$dados = array(
		'idUser'=>$id_user,
		'texto' =>$texto,
		'data' => $date,
			'status'=>1
		);
		$inserir = $this->db->insert('publicacao',$dados);	
		
		if($inserir){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function atualizarPublicacao($idPublicacao,$texto){
		$this->db->where('id',$idPublicacao);
		$atualizar = $this->db->update('publicacao',array('texto'=>$texto));
		
		if($atualizar){
			return true;
		}
		else{
			return false;
		}
	}
		public function deletarPublicacao($idPublicacao){
		$this->db->where('id',$idPublicacao);
		$atualizar = $this->db->update('publicacao',array('status'=>0));
		
		if($atualizar){
			return true;
		}
		else{
			return false;
		}
	}
	
	
}
?>