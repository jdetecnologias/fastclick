<?php

class AutocompletarPesquisa extends CI_Model{
	
	public function mostrarDados($search,$tabela,$coluna){
		$this->db->distinct($coluna);
		$this->db->group_by($coluna);
		$this->db->like($coluna,$search);
		$mostra = $this->db->get($tabela);
			if($mostra->num_rows()>0){
				return $mostra->result();
			}
		else{
			
			return false;
		}	
	}
	
	public function mostrarCategoria($search){
		$this->db->like('nome',$search);
		$mostra = $this->db->get('categorias');
		return $mostra->num_rows();
	}
	
	public function mostrarPorNome($search){
		$this->db->like('nome',$search);
		$mostra = $this->db->get('usuarioEmpresas');
		return $mostra->num_rows();
	}
	
	
}

?>