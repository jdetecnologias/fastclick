<?php

class Mgaleria extends CI_Model{
	
	public function getImages($id)	{
		$this->db->where("idUser",$id);
		$getImage = $this->db->get('image');
		
		if($getImage){
			return $getImage->result();
		}
		else{
			return false;
		}
	}
	
	public function getImageByIdPhoto($id){
		$this->db->where('id',$id);
		$getImage = $this->db->get('image');
		if($getImage){
			$getRow = $getImage->result();
			return $getImage->nome;
		}
	
	}
	
	
}

?>