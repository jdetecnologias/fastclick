<?php

class MobterPublicacao extends CI_Model{
	
	public function getPublicacao($id){
		$this->db->where('idUser',$id);
		$this->db->where('status',1);
		$this->db->order_by('id','DESC');
		$query = $this->db->get('publicacao');
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
		
	}
	
	
}

?>