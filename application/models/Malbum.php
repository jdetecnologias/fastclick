<?php

class Malbum extends CI_Model{
	
	public function getAlbuns($id)	{
		$this->db->where("idUser",$id);
		$this->db->distinct();
		$this->db->group_by("nome");
		$getAlbum = $this->db->get('album');
		
		if($getAlbum){
			return $getAlbum->result();
		}
		else{
			return false;
		}
	}
	
	public function getImageByIdPhoto($id){
		$this->db->where('id',$id);
		$getImage = $this->db->get('image');
		if($getImage){
			$getRow = $getImage->result();
			foreach($getRow as $linha){
			return $linha->nome;
				}
		}
	
	}
	
	
}

?>